<?php 
session_start();

if(isset($_SESSION['status'])){
 header("location: ../dashboard/"); 
}else{

 ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin - Login</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/alertify.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/alertify.rtl.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/themes/bootstrap.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/themes/bootstrap.rtl.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/themes/default.min.css" rel="stylesheet"> 
  <link href="../../js/alertifyjs/css/themes/default.rtl.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/themes/semantic.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/themes/semantic.rtl.min.css" rel="stylesheet">
</head>

<body style=" background-image: url('../../recursos/login.jpeg'); 
  background-attachment: fixed;
  background-repeat: no-repeat;
  background-size: 100% 100%;">

  <div class="container">


    <div class="card card-login mx-auto mt-5">
       <div class="card-header">Login SDCP</div>
      <center>
        <img src="../../recursos/logo.jpeg" width="150"alt="">
      </center>
      
     
      <div class="card-body">
        <form   >
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="usuario" class="form-control" placeholder="Usuario" required="required" autofocus="autofocus">
              <label for="usuario">Username</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="password" id="Password" class="form-control" placeholder="Password" required="required">
              <label for="Password">Contraseña</label>
            </div>
          </div>
          <div class="form-group">
            <div class="checkbox">
              <label>
                <input type="checkbox" value="remember-me">
                recordar contraseña
              </label>
            </div>
          </div>
        
                  </form>
                    <button class="btn btn-primary btn-block" onclick="log();">Login</button> 
        <div class="text-center">
          <a class="d-block small mt-3" href="../Registrar/index.php?#">Registrarse</a>
         <!--  <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
 -->        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
   <script src="../../js/login.js"></script>
 <script src="../../js/alertifyjs/alertify.min.js"></script>
</body>

</html>
<?php } ?>