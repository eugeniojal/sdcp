<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="../inicio/">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Nuevo</li>
</ol>


<div class="card-header">

 </div> 
<div class="card card-register mx-auto mt-5" style="background-image:url('../../recursos/logo.jpeg');
background-position:center;
background-repeat: no-repeat;">

    <div class="card-header">
<a href="../inicio/"><i class="fas fa-arrow-left "></i>&nbsp;Atras</a>
<center>
    Nueva Solicitud
</center>

  </div>
    <div class="card-body">
        <form>
            <div class="form-group">
                <div class="form-label-group">
                    <span>Titulo de Peticion</span>
                    <input type="text" id="titulo" class="form-control" placeholder="titulo" required="required">

                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-label-group">
                            <span>Poliza</span>
                            <select class="form-control js-example-basic-single" name="poliza" id="poliza" style="height:40px;!impotant">
                                <option value="">Seleccione una poliza</option>
                                <optgroup label="Automóvil ">
                                    <?php
                 while ($valores = mysqli_fetch_array($automovil)) {
                  echo '<option value="'.$valores['id'].'">'.$valores['nombre'].'</option>';
          } ?>
                                </optgroup>
                                <optgroup label="Salud ">
                                    <?php
                 while ($valores = mysqli_fetch_array($salud)) {
                  echo '<option value="'.$valores['id'].'">'.$valores['nombre'].'</option>';
          } ?>
                                </optgroup>
                                <optgroup label=" Accidentes Personales">
                                    <?php
                 while ($valores = mysqli_fetch_array($accidentes)) {
                  echo '<option value="'.$valores['id'].'">'.$valores['nombre'].'</option>';
          } ?>
                                </optgroup>
                                                                <optgroup label="Patrimoniales ">
                                    <?php
                 while ($valores = mysqli_fetch_array($patrimo)) {
                  echo '<option value="'.$valores['id'].'">'.$valores['nombre'].'</option>';
          } ?>
                                </optgroup>
                                <optgroup label="Servicios Funerarios ">
                                    <?php
                 while ($valores = mysqli_fetch_array($ser)) {
                  echo '<option value="'.$valores['id'].'">'.$valores['nombre'].'</option>';
          } ?>
                                </optgroup>


                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group">
                            <span>Tipo de peticion</span>
                            <select class="form-control js-example-basic-single" name="tpeticion" id="tpeticion" style="height:40px;!impotant">
                                <option value="">Tipo de solicitud</option>
                                <?php
                 while ($valores1 = mysqli_fetch_array($query1)) {
                  echo '<option value="'.$valores1['id'].'">'.$valores1['nombre'].'</option>';
          } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-label-group">
                    <span>Mensaje</span>
                    <textarea name="mensaje" id="mensaje" class="form-control" style="height: 92px;"></textarea>
                </div>
            </div>

        </form>
        <button class="btn btn-primary btn-block" onclick="Npeticion();">ENVIAR</button>
    </div>
</div>