<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Monitor</li>
</ol>

<!-- Icon Cards-->
<div class="row">
    <div class="col  mb-3">
        <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5">
                    <?php echo $soli." Solicitudes" ?>
                </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" id="solicitudes">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col  mb-3">
        <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5">
                    <?php echo $pre." Preguntas" ?>
                </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" id="preguntas">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col  mb-3">
        <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-shopping-cart"></i>
                </div>
                <div class="mr-5">
                    <?php echo $suge." Sugerencias" ?>
                </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" id="sugerencia">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col  mb-3">
        <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-life-ring"></i>
                </div>
                <div class="mr-5">
                    <?php echo $reclamo." Reclamos" ?>
                </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" id="reclamo">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col  mb-3">
        <div class="card text-white bg-secondary o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-life-ring"></i>
                </div>
                <div class="mr-5">
                    <?php echo $renovacion." renovacion" ?>
                </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" id="renovacion" >
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
</div>

<!-- tabla de peticiones  -->
<div id="DivTabla" class="card mb-3" style="display: none;">
    <div class="card-header">

        <i class="fas fa-table no-arrow"></i> PETICIONES

    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="1">
                <thead>
                    <tr>
                        <th>Nro</th>
                        <th>Usuario</th>
                        <th>TITULO</th>
                        <th>TIPO</th>
                        <th>POLIZA</th>
                        <th>MENSAJE</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody id="listaD" style="text-align: center;">

                </tbody>
                <tfoot>
                    <tr>
                        <th>Nro</th>
                        <th>Usuario</th>
                        <th>TITULO</th>
                        <th>TIPO</th>
                        <th>POLIZA</th>
                        <th>MENSAJE</th>
                        <th>OPCIONES</th>
                    </tr>
                </tfoot>

            </table>
        </div>
    </div>
    <div class="card-footer small text-muted">
        <?php echo "FECHA: ".date('d / m / o'); ?>
    </div>
</div>
<center>
    <img id="logo" src="../../recursos/logo.jpeg">
</center>