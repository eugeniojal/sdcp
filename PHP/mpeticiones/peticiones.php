  <input type="hidden" id="pag" value="2">
  <input type="hidden" id="use" value="<?php echo $usuario; ?>">
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="../inicio/">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Mis Peticiones</li>

    
</ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">

            <i class="fas fa-table no-arrow" ></i>
            Mis Peticiones
            <a class="nav-link" href="../new/" ><i class="fas fa-plus-square "></i> Agregar Peticion</a>
     
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Nro</th>
                    <th>Usuario</th>
                    <th>TITULO</th>
                    <th>TIPO</th>
                    <th>POLIZA</th>
                    <th>MENSAJE</th>
                    <th>ESTATUS</th>
                    <th>OPCIONES</th>
                  </tr>
                </thead>
                <tbody id="listape" style="text-align: center;">

                  </tbody>
                <tfoot>
                  <tr >
                    <th>Nro</th>
                    <th>Usuario</th>
                    <th>TITULO</th>
                    <th>TIPO</th>
                    <th>POLIZA</th>
                    <th>MENSAJE</th>
                    <th>ESTATUS</th>
                    <th>OPCIONES</th>
                  </tr>
                </tfoot>

              </table>
            </div>
          </div>
          <div class="card-footer small text-muted"><?php echo "FECHA: ".date('d / m / o'); ?></div>
        </div>
