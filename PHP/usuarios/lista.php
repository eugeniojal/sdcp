<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Usuarios</li>
</ol>

<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">

        <i class="fas fa-users"></i> Usuarios
        <?php 
if ($tipo == 3) {
  ?>
            <a class="nav-link" href="#" data-toggle="modal" data-target="#ModalUsuario"><i class="fas fa-user"></i>Agregar usuario</a>

            <?php
}

             ?>

    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nro</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO</th>
                        <th>NICK</th>
                        <th>TIPO USUARIO</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody id="listau" style="text-align: center;">

                </tbody>
                <tfoot>
                    <tr>
                        <th>Nro</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO</th>
                        <th>NICK</th>
                        <th>TIPO USUARIO</th>
                        <th>OPCIONES</th>
                    </tr>
                </tfoot>

            </table>
        </div>
    </div>
    <div class="card-footer small text-muted">
        <?php echo "FECHA: ".date('d / m / o'); ?>
    </div>
</div>

<div class="modal fade" id="ficha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-body">
            <div class="card" >
              <center>
                <img src="../../recursos/user.png" width="150px" height="150px" alt="...">
              </center>
              <div id="info-ficha">
                
              </div>
                
                <div class="card-body">
                    <button style="float:right;" class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>

    </div>
</div>