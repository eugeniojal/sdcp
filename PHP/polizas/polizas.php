  <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Tables</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">

            <i class="fas fa-table no-arrow" ></i>
            Polizas
            <?php   

              if ($_SESSION['user_tipo'] == 3) {
                ?>
                <a class="nav-link" href="#" data-toggle="modal" data-target="#AddPolizaModal"><i class="fas fa-plus-square "></i>Agregar poliza</a>
            <?php
              }

             ?>
           
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Nro</th>
                    <th>NOMBRE DE POLIZA</th>
                    <th>RAMA DE POLIZA</th>
                    <th>OPCIONES</th>
                  </tr>
                </thead>
                                  <tbody id="listap" style="text-align: center;">

                  </tbody>
                <tfoot>
                  <tr >
                    <th>Nro</th>
                    <th>NOMBRE DE POLIZA</th>
                    <th>RAMA DE POLIZA</th>
                    <th>OPCIONES</th>
                  </tr>
                </tfoot>

              </table>
            </div>
          </div>
           <div class="card-footer small text-muted"><?php echo "FECHA: ".date('d / m / o'); ?></div>
        </div>


