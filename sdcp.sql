-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-11-2019 a las 17:56:49
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sdcp`
--
CREATE DATABASE IF NOT EXISTS `sdcp` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `sdcp`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(10) NOT NULL,
  `msg` text COLLATE utf8_spanish_ci NOT NULL,
  `usr` int(10) NOT NULL,
  `peticion` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `msg`, `usr`, `peticion`) VALUES
(15, 'BUENOS DIAS YA ESTAMOS PROCESANDO SU MENSAJE', 1, 1),
(16, 'BUENOS DIAS', 1, 3),
(17, 'HOLesyrdytu', 1, 3),
(18, 'MAMALO', 8, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peticion`
--

CREATE TABLE `peticion` (
  `id` int(10) NOT NULL,
  `titulo` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` int(10) NOT NULL,
  `poliza` int(10) NOT NULL,
  `usuario` int(10) NOT NULL,
  `mensaje` text COLLATE utf8_spanish_ci NOT NULL,
  `estatus` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'ABIERTO',
  `usr_resp` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `peticion`
--

INSERT INTO `peticion` (`id`, `titulo`, `tipo`, `poliza`, `usuario`, `mensaje`, `estatus`, `usr_resp`) VALUES
(7, 'Necesito Ayuda', 1, 47, 1, 'Necesito ayuda con mi poliza', 'ABIERTO', 0);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `peticionestotales`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `peticionestotales` (
`id` int(10)
,`nick` varchar(350)
,`titulo` varchar(350)
,`tipo` varchar(350)
,`poliza` varchar(350)
,`mensaje` text
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `polizas`
--

CREATE TABLE `polizas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `rama` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `polizas`
--

INSERT INTO `polizas` (`id`, `nombre`, `rama`) VALUES
(47, 'Responsabilidad civil', 'AutomÃ³vil'),
(49, 'PÃ©rdida total', 'AutomÃ³vil '),
(50, 'Cobertura amplia', 'AutomÃ³vil'),
(51, 'Individual', 'Salud'),
(52, 'Colectiva (empresa, persona jurÃ­dica)', 'Salud'),
(53, 'Individual', 'Accidentes Personales'),
(54, 'Colectiva (empresa, persona jurÃ­dica)', 'Accidentes Personales'),
(55, 'Individual', 'Servicios  Funerarios'),
(56, 'Colectiva (empresa, persona jurÃ­dica)', 'Servicios  Funerarios'),
(57, 'Incendio y Responsabilidad Civil', 'Patrimoniales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tp`
--

CREATE TABLE `tp` (
  `id` int(10) NOT NULL,
  `nombre` varchar(350) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tp`
--

INSERT INTO `tp` (`id`, `nombre`) VALUES
(1, 'RECLAMO'),
(3, 'SOLICITUD'),
(4, 'PREGUNTA'),
(5, 'SUGERENCIA'),
(6, 'RENOVACION');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL,
  `tipo` int(10) NOT NULL DEFAULT '1',
  `nick` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `pw` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `ci` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `tlf` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `tipo`, `nick`, `pw`, `nombre`, `apellido`, `ci`, `tlf`, `correo`) VALUES
(1, 3, 'admin', '123456', 'Administrador', 'Principal', '', '', ''),
(8, 1, 'empleado', '123456', 'empleado', 'ampleado', '1235456', '123456', 'eueuusuka@gmail.com'),
(9, 1, 'usuario', '123456', 'usuario121211', 'usuario', '', '', ''),
(57, 1, 'eugeniojal1', 'espiderman', 'EUGENIO JOSE', 'ANTON LUNAR', '25897751', '04143663615', 'eugeniojal1@gmail.com'),
(58, 3, 'jaguilar', '123456', 'jonathan', 'aguilar', '8977511', '04149804412', 'jaguilar@gmail.com');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_peti`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_peti` (
`id` int(10)
,`nick` varchar(350)
,`titulo` varchar(350)
,`tipo` varchar(350)
,`poliza` varchar(350)
,`mensaje` text
,`estatus` varchar(50)
,`usuid` int(10)
,`rama` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_peticiones`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_peticiones` (
`id` int(10)
,`nick` varchar(350)
,`titulo` varchar(350)
,`tipo` varchar(350)
,`poliza` varchar(350)
,`mensaje` text
,`estatus` varchar(50)
,`usuid` int(10)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `peticionestotales`
--
DROP TABLE IF EXISTS `peticionestotales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `peticionestotales`  AS  select `pt`.`id` AS `id`,`us`.`nick` AS `nick`,`pt`.`titulo` AS `titulo`,`tp`.`nombre` AS `tipo`,`po`.`nombre` AS `poliza`,`pt`.`mensaje` AS `mensaje` from (((`peticion` `pt` join `tp` on((`tp`.`id` = `pt`.`tipo`))) join `polizas` `po` on((`po`.`id` = `pt`.`poliza`))) join `usuarios` `us` on((`us`.`id` = `pt`.`usuario`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_peti`
--
DROP TABLE IF EXISTS `vista_peti`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_peti`  AS  select `pt`.`id` AS `id`,`us`.`nick` AS `nick`,`pt`.`titulo` AS `titulo`,`tp`.`nombre` AS `tipo`,`po`.`nombre` AS `poliza`,`pt`.`mensaje` AS `mensaje`,`pt`.`estatus` AS `estatus`,`us`.`id` AS `usuid`,`po`.`rama` AS `rama` from (((`peticion` `pt` join `usuarios` `us` on((`us`.`id` = `pt`.`usuario`))) join `tp` on((`tp`.`id` = `pt`.`tipo`))) join `polizas` `po` on((`po`.`id` = `pt`.`poliza`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_peticiones`
--
DROP TABLE IF EXISTS `vista_peticiones`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_peticiones`  AS  select `pt`.`id` AS `id`,`us`.`nick` AS `nick`,`pt`.`titulo` AS `titulo`,`tp`.`nombre` AS `tipo`,`po`.`nombre` AS `poliza`,`pt`.`mensaje` AS `mensaje`,`pt`.`estatus` AS `estatus`,`us`.`id` AS `usuid` from (((`peticion` `pt` join `usuarios` `us` on((`us`.`id` = `pt`.`usuario`))) join `tp` on((`tp`.`id` = `pt`.`tipo`))) join `polizas` `po` on((`po`.`id` = `pt`.`poliza`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `peticion`
--
ALTER TABLE `peticion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `polizas`
--
ALTER TABLE `polizas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tp`
--
ALTER TABLE `tp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `peticion`
--
ALTER TABLE `peticion`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `polizas`
--
ALTER TABLE `polizas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `tp`
--
ALTER TABLE `tp`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
