<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>S.R.G.S.I.R</title>
   <link rel="shortcut icon" href="../../recursos/logo.jpeg" />
  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/select2.min.css" rel="stylesheet">
  
 <!-- ALERTIFY-->
  <link href="../../js/alertifyjs/css/alertify.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/alertify.rtl.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/themes/bootstrap.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/themes/bootstrap.rtl.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/themes/default.min.css" rel="stylesheet"> 
  <link href="../../js/alertifyjs/css/themes/default.rtl.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/themes/semantic.min.css" rel="stylesheet">
  <link href="../../js/alertifyjs/css/themes/semantic.rtl.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
    <img src="../../recursos/logo.jpeg" width="60px"alt="">
    &nbsp;

    <a class="navbar-brand mr-1" href="../inicio/">S.R.G.S.I.R</a>
  
    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
<!--<div class="input-group">
        <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div> -->
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
<!--       <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-bell fa-fw"></i>
          <span class="badge badge-danger">9+</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li> -->
<!--       <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-envelope fa-fw"></i>
          <span class="badge badge-danger"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li> -->
      <li class="nav-item dropdown no-arrow">
         <p class="nav-link dropdown-toggle"><strong><?php echo $_SESSION['user_nombre']." ".$_SESSION['user_apellido'];?></strong></p>
      </li>
      
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw fa-3x" ></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" style="text-align: center;"aria-labelledby="userDropdown">
<!--           <a class="dropdown-item" href="#">Settings</a>
          <a class="dropdown-item" href="#">Activity Log</a> -->
          <p><strong><?php echo $_SESSION['user_nombre'] ?></strong></p>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">

        <?php 
    if ( $_SESSION['user_tipo'] != 1) {
      
  ?> 

  <li class="nav-item">
        <a class="nav-link" href="../dashboard">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="../new">
          <i class="fas fa-plus-square "></i>
          <span>Nuevo</span></a>
      </li>

     <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Peticiones</span>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Lista de Peticiones</h6>
          <a class="dropdown-item" href="../mpeticiones">Mis Peticiones</a>
          <a class="dropdown-item" href="../peticiones">Todas</a>
        </div>
      </li>
          <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <i class="fas fa-users"></i>
          <span>Usuarios</span>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Lista</h6>
          <a class="dropdown-item" href="../usuarios/">Todos los usuarios</a>
        </div>
      </li>
          <li class="nav-item">
        <a class="nav-link" href="../polizas">
          <i class="far fa-address-card"></i>
          <span>Polizas</span></a>

      </li>
  <?php  

    }else{
    ?>
    <li class="nav-item">
        <a class="nav-link" href="../new">
          <i class="fas fa-plus-square "></i>
          <span>Nuevo</span></a>
      </li>
           <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Peticiones</span>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Lista de Peticiones</h6>
          <a class="dropdown-item" href="../mpeticiones">Mis Peticiones</a>
          <!-- <a class="dropdown-item" href="../peticiones">Todas</a> -->
        </div>
      </li>

<?php 
    }
     ?>
  
 


    </ul>
   <div id="content-wrapper">
 <div class="container-fluid">