       <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Bienvenidos a ACorvino C.A Tlf: 0212.242.14.50 Correo: antonietacorvino@gmail.com</span>
          </div>
        </div>
      </footer>
 
</div>
    </div>
 <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Listo para salir?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Estas seguro que decea abandonar la pagina</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" onclick="logout();">Logout</button>
        </div>
      </div>
    </div>
  </div>
  <!--MODAL NUEVA POLIZA-->
    <div class="modal fade" id="AddPolizaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Poliza</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-group">
            <div class="form-label-group" >
               <span >NOMBRE DE POLIZA</span>
              <input type="text" id="NPoliza" class="form-control" placeholder="NOMBRE DE POLIZA" required="required">
            </div>
          </div>
        <div class="form-group">
            <div class="form-label-group" >
           
           <span >RAMA DE POLIZA</span>
           <select name="NPoliza" id="RPoliza" class="form-control"  >
             <option value="Automóvil">Automóvil </option>
             <option value="Salud">Salud</option>
             <option value="Accidentes Personales">Accidentes Personales</option>
             <option value="Servicios  Funerarios">Servicios  Funerarios</option>
             <option value="Patrimoniales">Patrimoniales</option>
           </select>

            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" onclick="guardarp();">GUARDAR</button>
        </div>
      </div>
    </div>
  </div>
  <!-- EDITAR POLIZA -->
   <div class="modal fade" id="EditPolizaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Editar Poliza</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div id="modalp">
          
        </div>
      </div>
    </div>
  </div>

  <!--MODAL AGREGAR USUARIO-->
    <div class="modal fade" id="ModalUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Usuario</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-group">
            <div class="form-label-group" >
               <span >NOMBRE DE USUARIO</span>
              <input type="text" id="NombreU" class="form-control" placeholder="NOMBRE" required="required">
            </div>
          </div>
         <div class="form-group">
            <div class="form-label-group" >
               <span >APELLIDO DE USUARIO</span>
              <input type="text" id="ApellidoU" class="form-control" placeholder="Apellido" required="required">
            </div>
          </div>
         <div class="form-group">
            <div class="form-label-group" >
               <span >NICK</span>
              <input type="text" id="Nick" class="form-control" placeholder="Apodo" required="required">
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group" >
               <span >CI/RIF</span>
              <input type="text" id="CI" class="form-control" placeholder="CI/RIF" required="required">
            </div>
          </div>
        <div class="form-group">
            <div class="form-label-group" >
               <span >TLF</span>
              <input type="text" id="TLF" class="form-control" placeholder="TLF" required="required" >
            </div>
          </div>
        <div class="form-group">
            <div class="form-label-group" >
               <span >Correo</span>
              <input type="text" id="CORREO" class="form-control" placeholder="Correo" required="required">
            </div>
          </div>
         <div class="form-group">
            <div class="form-label-group" >
               <span >CONTRASEÑA</span>
              <input type="password" id="Passw" class="form-control" placeholder="Contraseña" required="required">
            </div>
          </div>
       <div class="form-group">
            <div class="form-label-group" >
               <span >TIPO DE USUARIO</span>
           <select name="TipoU" id="TipoU" class="form-control"  >
             <option value="1">Usuario</option>
             <option value="2">Empleado</option>
             <option value="3">Administrador</option>
             option
           </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" onclick="guardaru();">GUARDAR</button>
        </div>
      </div>
    </div>
  </div>
  <!-- MODAL EDITAR USUARIO -->
   <div class="modal fade" id="ModalUsuarioE" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div id="modale">
          
        </div>
        
        
       

      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->


  <!-- Page level plugin JavaScript-->


  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="../../js/demo/datatables-demo.js"></script>
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/alertifyjs/alertify.min.js"></script>
  <script src="../../js/select2.min.js"></script>
 <script src="../../js/logout.js"></script>



<?php if ($pagina==0) {
?>  
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="../../js/new.js"></script>
    <script src="../../vendor/chart.js/Chart.min.js"></script>
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
<?php
} ?>

<?php if ($pagina==1) {
?>  
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="../../js/polizas.js"></script>
    <script src="../../vendor/chart.js/Chart.min.js"></script>
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
<?php
} ?>
<?php if ($pagina==2) {
?>  
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="../../js/peticiones.js"></script>
    <script src="../../vendor/chart.js/Chart.min.js"></script>
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
<?php
} ?>
<?php if ($pagina==3) {

?>  
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="../../js/usuarios.js"></script>
    <script src="../../vendor/chart.js/Chart.min.js"></script>
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
<?php
} ?>
<?php if ($pagina==4) {
?>  
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="../../js/dash.js"></script>
    <script src="../../vendor/chart.js/Chart.min.js"></script>
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
<?php
} ?>
<?php if ($pagina==5) {
?>  
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="../../js/msg.js"></script>
    <script src="../../vendor/chart.js/Chart.min.js"></script>
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
<?php
} ?>
<?php if ($pagina==6) {
?>  
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="../../js/mpeticiones.js"></script>
    <script src="../../vendor/chart.js/Chart.min.js"></script>
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
<?php
} ?>
</body>

</html>